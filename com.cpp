

#include<iostream>
using namespace std;

class Complex
{
  private: 
    int real;
    int img;
  public: 
    friend istream & operator>>(istream &in, Complex &c);
    friend ostream & operator<<(ostream &out, Complex &c);
};

istream & operator>>(istream &in, Complex &c)
{
  cout << "Enter the value of Real Portion : ";
  in >> c.real;

  cout << "Enter the value of Imaginary Portion : ";
  in >> c.img;

  return in;
}

ostream & operator<<(ostream &out, Complex &c)
{
  out << c.real << " + " << c.img << "i" << endl;
  return out;
}

int main()
{
  Complex c;

  cin >> c;
  cout << c;
}
