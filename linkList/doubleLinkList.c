#include<stdio.h>
#include<stdlib.h>

struct node {

    int info;
    struct node *next;
    struct node *prev;
};
struct node *getnode(int value) {

    struct node * temp = NULL;

    temp = malloc(sizeof(struct node*));
    temp->info = value;
    temp->next = NULL;
    temp->prev = NULL;
    return temp;
}
void initList(struct node **h) {

    (*h) = getnode(0);
}
void append(struct node *h,int value) {

    // struct node *q = h;
    struct node *p = h;
    struct node *temp = getnode(value);

    while(p->next!=NULL) {

        // q = p;
        p = p->next;
    }
    p->next = temp;
    h->info++;
}
void display(struct node *h){

    struct node *p = h->next;

    while(p) {

        printf(" %d ",p->info);
        p = p->next;
    }


}
void freeNode(struct node *p) {

    free(p);
}
int delete(struct node *h,int pos) {

    struct node *q = h;
    struct node *p = h->next;

    int i;

    if(pos > h->info) {

        // printf("\nInvalid position\n");
        // exit(0);
        return 1;
    }

    for(i = 0;i<pos-1;i++) {

        q = p;
        p = p->next;
    }
    q->next = p->next;
    p->prev = NULL;
    freeNode(p);
    // printf("Deleted successfully!!...");
    h->info--;
    return 0;
    
}
int insert(struct node *h, int pos,int value) {

    struct node *q = h;
    struct node *p = h->next;
    struct node *temp = getnode(value);

    int i;

    if(pos>h->info)
        return 1;

    for(i=0;i<pos-1;i++) {

        q = p;
        p = p->next;
    }    

    q->next = temp;
    temp->next = p;
    temp->prev = q;
    p->prev = temp;

    h->info++;
    return 0;

}
int search(struct node *h, int value) {

    struct node *p = h->next;

    while(p) {

        if(p->info == value)
            return 0;
        p = p->next;    
    }
    return 1;
}
int main() {

    struct node *head = NULL;
    int ch,value;

    initList(&head);

    while(1) {

        printf("\nEnter the choice : \t");
        printf("\n1: append 2: Display 3:Delete 4: Insert  5:Search :   \t \n");
        scanf("%d",&ch);

        switch(ch){

            case 1: printf("Enter the value to append : \t");
                    scanf("%d",&value);
                    append(head,value);
                    break;
            case 2: display(head);
                    break;  
            case 3: printf("\nEnter the position : \t");
                    scanf("%d",&value);
                    // exit(0);
                    ch = delete(head,value);  
                    if(ch == 1)
                        printf("\nInvaid position!!\n"); 
                    else
                        printf("\ndeleted successfully!!\n");              
                    break;
            case 4: printf("\nEnter the postion where you want to insert : \t");
                    scanf("%d",&ch);
                    printf("\nEnter the value");
                    scanf("%d",&value);
                    ch = insert(head,ch,value);
                    if(ch == 1)
                        printf("\nInvalid position!!!...Try again\n");
                    else
                        printf("\nInserted successfully!!!\n");    
                    break; 
            case 5: printf("\nEnter the element for search : \t");
                    scanf("%d",&value);
                    ch = search(head,value);
                    if(ch == 0)
                        printf("\nElement found!!\n");
                    else
                        printf("Element not found!!\n");    
                    break;             

        }    
    }
}