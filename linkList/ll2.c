#include<stdio.h>
#include<stdlib.h>

struct node {

    int info;
    struct node *next;
};
struct node *getnode(int value) {

    struct node *temp = NULL;
    temp = malloc(sizeof(struct node *));
    temp->info = value;
    temp->next = NULL;
    return temp; 
}
void initList(struct node **head) {

    *head = getnode(0);
}
void insert(struct node *head,int value) {

    struct node *q = head;
    struct node *p = head->next;
    struct node *temp = NULL;

    while(p && p->info <= value) {

        // printf("\n%d\n",p);
        // printf("\n%u\n",&p);
        q=p;
        p = p->next;
    }
    temp = getnode(value);
    q->next = temp;
    temp->next = p;
    head->info++;


}
void display(struct node *head) {

    struct node *p = head->next;
    while(p) {

        printf(" %d ",p->info);
        p = p->next;
    }
}
int search(struct node *head,int value) {

    struct node *p = head->next;

    while(p) {

        if(p->info == value)
            return value;
        p = p->next;    
    }
    return -1;

}
int main() {

    struct node *head = NULL;
    int ch,value;

    initList(&head);

    while(1) {
        printf("\nEnter the choice : 1: Insert  2:Display  3:Search \t");
        scanf("%d", &ch);

        switch(ch) {

            case 1: printf("\nEnter value : \t");
                    scanf("%d",&value);
                    insert(head,value);
                    break;
            case 2: display(head);
                    break; 
            case 3: printf("ENter value for search : \t");
                    scanf("%d",&value);
                    ch = search(head,value);
                    if(ch == value)
                        printf("\nKey found : %d\n",value);   
                    else
                        printf("\nKey not found!!\n");    
                    break;               
        }
    }
}