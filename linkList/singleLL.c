#include<stdio.h>
#include<stdlib.h>

struct node {

    int info;
    struct node *next;
};
struct node *getNode(int value) {

    struct node *temp = NULL;
    temp = (malloc(sizeof(struct node *)));
    temp->info = value;
    temp->next = NULL;
    return temp;
}
void initList(struct node **h) {

    (*h) = getNode(0);
}
void append(struct node *h, int value) {

    // struct node *p = h->next;

    h->info++;
    while(h->next!=NULL) {

        h = h->next;
    }
    h->next = getNode(value);
}
void insert(struct node *h,int value,int pos) {

    struct node *q = h;
    struct node *p = h->next;
    struct node *temp = getNode(value);
    int i;
    if(pos > h->info) {

        printf("Invaid Position!!");
        exit(1);
    }

    for(i=0;i<pos-1;i++) {

        q=p;
        p=p->next;
    }
    q->next = temp;
    temp->next = p;
    h->info++;
}
void display(struct node *h) {

    struct node *p = h->next;
    
    while(p) {

        printf("  %d  ",p->info);
        p = p->next;

    }
}
int search(struct node *h,int value) {

    struct node *p = h->next;
    
    while(p) {

        if(p->info == value)
            return p->info;
        p = p->next;    
    }
    return -1;
}
void freeNode(struct node *p) {

    free(p);
}
void delete(struct node * head,int pos) {

    struct node *q = head;
    struct node *p = head->next;
 

    int i;
    if(pos > head->info) {
    
        printf("\nInvalid position\n");
        exit(1);
    }
    for(i = 0;i < pos-1;i++) {

        q = p;
        p = p->next;
    }
    q->next = p->next;
    freeNode(p);
    head->info--;

}
int main() {

    struct node *head = NULL;

    int ch,value;
    initList(&head);
    while(1) {
        printf("\nENter choice :\t \n1: Insert  2:Display 3:search 4:delete  5:Append :\t");
        scanf("%d",&ch);

        

            switch(ch) {
                case 1: printf("Enter the postion :\t");
                        scanf("%d",&ch);
                        printf("Enter the value :\t");
                        scanf("%d",&value);
                        insert(head,value,ch);
                        break;
                case 2: display(head);
                        break;     
                case 3: printf("Enter the element for search : \t");
                        scanf("%d",&value);
                        ch = search(head,value);

                        if(ch == value)
                            printf("\nKey found!!\n");
                        else
                            printf("\nKey not found\n");    
                        break;
                case 4: printf("\nEnter the position to delete : \t");
                        scanf("%d",&value);
                        delete(head,value);
                        printf("deleted successfully...!");
                        break;      
                case 5: printf("Enter the value : \t");
                        scanf("%d",&value); 
                        append(head,value);
                        break;   
                               

            }            
    }
}