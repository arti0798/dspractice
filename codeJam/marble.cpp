#include<iostream>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;

const int N=2020;
int h[N],e[N],ne[N],idx;
int n;
int a[N];
char s[N];
bool st[N];


void add(int a,int b){
    e[idx]=b,ne[idx]=h[a],h[a]=idx++;
}
char getneg(char d){
    if(d=='L') return 'R';
    else return 'L';
}

void bfs(int x){
    queue<int> q;
    s[x]='L';
    q.push(x);
    st[x]=true;

    while(q.size()){
        int t=q.front();
        q.pop();
        for(int i=h[t];i!=-1;i=ne[i]){
            int j=e[i];
            if(!st[j]){
                s[j]=getneg(s[t]);
                q.push(j);
                st[j]=true;
            }
        }
    }


}
int main(){

    int T,casei=0;
    cin>>T;
    while(T--){
        
        idx=1;
        memset(h,-1,sizeof h);
        memset(st,0,sizeof st);
        casei++;
        cin>>n;

        
        for(int i=1;i<=n;i++) {
            cin>>a[i];
            if(i%2==0){
                add(i,i-1);
                add(i-1,i);
            }
        }
        for(int i=n;i>0;i-=2){
            add(a[i],a[i-1]);
            add(a[i-1],a[i]);
        }

        for(int i=1;i<=n;i++){
            if(st[i]==false){
                bfs(i);
            }
        }


        cout<<"Case #"<<casei<<": ";
        for(int i=1;i<=n;i++){
            cout<<s[i];
        }
        cout<<endl;

    }
    return 0;
}
